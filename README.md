[![License](https://img.shields.io/badge/license-MIT-red.svg)](http://choosealicense.com/licenses/mit/)

Flask based experiment runner for web-based self paced reading experiments originally written for BCS152 Fall Semester 2014.

[![Flask powered](http://flask.pocoo.org/static/badges/flask-powered.png)](http://flask.pocoo.org/)

## Requirements
  * Flask
  * SQLAlchemy
  * Six
  * Boto

## Installing

These instructions assume you are using Apache. After I get it installed using NGinx,
I'll write that up too.

  1. Create a virtualenv somewhere and clone the repository within it. I use `/usr/local/mturk-apps/` as the base directory for all my Python web apps, so that's what code examples will use, but choose a directory that works for you. I've not tested with Python3, but in theory using Six I've made it portable between 2 and 3.
  2. After activating your virtualenv, run `pip install -r requirements.txt`
  3. Install the appropriate Python library for connecting to your database of choice and edit `SQLALCHEMY_DATABASE_URI` in `config.py` with the URI for your particular database. For production, use a real database that supports locking like [PostgreSQL](http://www.postgresql.org/) or [MariaDB](https://mariadb.org/) / [MySQL](https://www.mysql.com/), not [SQLite](https://www.sqlite.org/).
  4. Run `python db_create.py` to initialize the database.
  5. Copy `flaskspr.wsgi` to wherever you keep your WSGI scripts. Set the value of `PROJECT_DIR` to the virtualenv where you installed the app (not the app directory within).
  6. Either use the REPL or `add_experiment.py` to add your experiment(s) and list(s).
  7. If you plan to use the Mechanical Turk [Notifications](http://docs.aws.amazon.com/AWSMechTurk/latest/AWSMechanicalTurkRequester/Concepts_NotificationsArticle.html) or Google Maps and geolocation, be sure to set the values in `config.py` for the following:
```bash
AWS_ACCESS_KEY = ''
AWS_SECRET_KEY = ''
SQS_REGION = ''
SQS_QUEUE = ''
GMAPS_API_KEY = ''
```
  