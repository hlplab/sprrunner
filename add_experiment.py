#!../bin/python

import argparse
from config import SQLALCHEMY_DATABASE_URI
from app.models import Experiment, TrialList

parser = argparse.ArgumentParser(description='Add an experiment and its lists')
parser.add_argument('-e', '--experiment', required=True)
parser.add_argument('-c', '--count', type=int, required=True)
args = parser.parse_args()

expt, e_existed = Experiment.get_one_or_create(name=args.experiment)
for i in range(1, args.count + 1):
    newlist, tl_existed = TrialList.get_one_or_create(name='list{}'.format(i), number=i, experiment=expt)
