#!/usr/bin/env python

#
# Author: Andrew Watts <awatts2@ur.rochester.edu>
#
# Copyright (c) 2014-2016, Andrew Watts and
#        the University of Rochester BCS Department
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import logging
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_admin import Admin
from .ReverseProxyMiddleWare import ReverseProxied
from flask_queryinspect import QueryInspect
from flask_security import Security, SQLAlchemyUserDatastore

logging.basicConfig(format='%(asctime)-15s %(levelname)s %(message)s')
logging.getLogger('flask_queryinspect').setLevel(logging.DEBUG)

app = Flask(__name__)
app.config.from_object('config')

db = SQLAlchemy(app)
qi = QueryInspect(app)
admin = Admin(app)

# n.b. pylint will complain about an imports down here,
# but they have to be after app and db are defined or everything breaks

# Setup Flask-Security
from app.models import User, Role
user_datastore = SQLAlchemyUserDatastore(db, User, Role)
security = Security(app, user_datastore)

# http://flask-admin.readthedocs.io/en/latest/introduction/#customizing-builtin-views
# define a context processor for merging flask-admin's template context into the
# flask-security views.
#@security.context_processor
#def security_context_processor():
#    return dict(admin_base_template=admin.base_template,
#                admin_view=admin.index_view,
#                h=admin_helpers,
#                get_url=url_for)

app.wsgi_app = ReverseProxied(app.wsgi_app)

from app.api.views import mod as api_mod
from app.sprrunner.views import mod as spr_mod
from app.socalign.views import mod as socalign_mod
from app.common.views import mod as commmon_mod

app.register_blueprint(api_mod)
app.register_blueprint(spr_mod)
app.register_blueprint(socalign_mod)
app.register_blueprint(commmon_mod)
