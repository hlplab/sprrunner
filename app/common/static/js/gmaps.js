var hlpgmaps = (function($, undefined){
    var mylat, mylong, myacc;

    var options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
    };

    function error(err) {
        console.warn('ERROR(' + err.code + '): ' + err.message);
        $('[name="badlocation"]').prop('checked', true);
        $('[name="badlocation"]').attr('disabled', 'disabled');
        $('[name="reportedlocation"]').removeAttr('disabled');
        $('[name="reportedlocation"]').focus();
        switch(err.code) {
            case 1: // (1) permission denied
                $('[name="location"]').val('Geolocation permission denied');
                $('[name="reportedlocation"]').after('<p style=color:red;font-weight:bold;font-size:x-large>You have denied permission for geolocation. Please provide your location as "City, State".</p>');
                break;
            case 2: // (2) position unavailable
                $('[name="location"]').val('Geolocation unavailable');
                $('[name="reportedlocation"]').after('<p style=color:red;font-weight:bold;font-size:x-large>Geolocation unavailable. Please provide your location as "City, State".</p>');
                break;
            case 3: // (3) timeout
                $('[name="location"]').val('Geolocation timed out');
                $('[name="reportedlocation"]').after('<p style=color:red;font-weight:bold;font-size:x-large>Geolocation has timed out. Please provide your location as "City, State".</p>');
                break;
        }
        // Fill in map slot with North America centered on geographic center of US
        $('#location').attr('src',
            "https://maps.googleapis.com/maps/api/staticmap?center=39.5,-98.35&zoom=3&size=600x400&sensor=false&maptype=roadmap");
        $('button#dismiss_map').removeAttr('disabled');
    }

    function static_map() {
        $('#location').attr('src',
            "https://maps.googleapis.com/maps/api/staticmap?center=" +
            mylat + "," + mylong +
            "&zoom=8&size=600x400&sensor=false&maptype=roadmap&markers=color:red|" +
            mylat + "," + mylong);

    }

    function codeLatLng() {
        var myLatLng = new google.maps.LatLng(mylat, mylong);
        var geocoder = new google.maps.Geocoder();
        var location;
        geocoder.geocode({'latLng': myLatLng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                $('input[name="location"]').val([mylat,mylong,myacc,results[3].formatted_address.replace(/,/g, ';')].join('|'));
            } else {
                $('input[name="location"]').val([mylat,mylong,myacc].join('|'));
            }
        });
    }

    return {
        getLocation: function () {
            navigator.geolocation.getCurrentPosition(function (pos) {
                mylat = pos.coords.latitude;
                mylong = pos.coords.longitude;
                myacc = pos.coords.accuracy;
                static_map();
                codeLatLng();
                $('button#dismiss_map').removeAttr('disabled');
            }, error, options);
        }
    }
}(jQuery));

// XXX: code not currently in use
// var infowindow = new google.maps.InfoWindow();
// function initialize() {
//   myLatLng = new google.maps.LatLng(mylat, mylong);
//   geocoder = new google.maps.Geocoder();
//   var mapOptions = {
//     center: myLatLng,
//     zoom: 8,
//     disableDefaultUI: true,
//     mapTypeId: google.maps.MapTypeId.ROADMAP
//   };
//   var map = new google.maps.Map(document.getElementById("map_canvas"),
//       mapOptions);
//   var marker = new google.maps.Marker({
//       position: myLatLng,
//       title:"Hello World!",
//       animation: google.maps.Animation.DROP
//       });
//   var circoptions = {
//       strokeColor: "#0000FF",
//       strokeOpacity: 0.3,
//       strokeWeight: 2,
//       fillColor: "#0000FF",
//       fillOpacity: 0.3,
//       map: map,
//       center: myLatLng,
//       radius: myacc
//   };
//   var acc_circle = new google.maps.Circle(circoptions);
//   marker.setMap(map);
//   codeLatLng();
// }
// function codeLatLng() {
//   geocoder.geocode({'latLng': myLatLng}, function(results, status) {
//       if (status == google.maps.GeocoderStatus.OK) {
//           infowindow.setContent(results[3].formatted_address);
//           infowindow.open(map, marker);
//       } else {
//           alert("Geocoder failed due to: " + status);
//       }
//   });
// }
