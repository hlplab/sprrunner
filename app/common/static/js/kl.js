/* jshint jquery: true, browser: true, devel: true, strict: false , quotmark: false*/
/* global _, swfobject */

function register(field, answer) {
    var resultform = $('#results');
    resultform.append('<input type="hidden" name="' + field + '" id="' + field + '" value="' + answer + '" /> ');
    console.log("Field: " + field + ", Answer: " + answer);
    return true;
}

function log(args) {
    if (typeof console !== "undefined" && typeof console.log !== "undefined") {
        console.log(args);
    }
}

function onExperimentEnd() {
    $('object').css('visibility', 'hidden');
    $('#content').hide();
    $('#langbackground').show();
    $('#langbackground li:first').show();
}

var flashref = null;
function flashLoadedCB(e) {
    if (e.success) {
        flashref = e.ref;
    } else {
        $('#flashcontent').html('Failed to load flash content.');
    }
}

var flashvars = {script: window.experiment_script, path:'/mturk/fedzech/KL/';
var params = {allowScriptAccess: "always", menu: "false", bgcolor:"#ffffff", quality: "high"};

$(document).ready(function() {

    $('input[name="browserid"]').val(navigator.userAgent);
    $('input[name="flashversion"]').val(swfobject.ua.pv.join("."));

    $('#content').hide();
    $('#endinstr').on('click', function() {
      $('#instructions').hide(function() {
          swfobject.embedSWF("https://www.hlp.rochester.edu/mturk/fedzech/allapp_flex_4.1_endtrigger.swf",
              "flashcontent", "900", "600", "12.0.0", null,
              flashvars, params, {}, function(e) {flashLoadedCB(e);});
        $('#content').show();
      });
    });

    $('input#consentcx').on('change', function() {
        $('#content').hide();
        $('#instructions').hide();
        $('#content').show();
    });

    $('button').button();

    //$('.multichoice, .scaleq').on('change', function() {$(this).siblings('.nextq').button('enable');});
    //$('.surveyq textarea').on('input', function () {
    //  if ($(this).val() !== '') {
    //    $(this).parent().siblings('.nextq').button('enable');
    //  } else {
    //    $(this).parent().siblings('.nextq').button('disable');
    //  }
    //});
    //
    ///*
    // * Lang background button handlers
    // */
    //_.each(_.initial($('#langbackground .nextq')), function(e) {
    //  $(e).on('click', function() {
    //      $(this).parent().hide('slide', function() {
    //        $(this).next().show();
    //    });
    //  });
    //});
    //
    //$('#langbackground .nextq:last').on('click', function() {
    //  $('#langbackground').hide('slide', function() {
    //    $('#rsrb').show('slide');
    //  });
    //});
    //
    ///*
    // * Special cases where one question has two parts
    //*/
    //$('[name="language.howlearn"], [name="language.overallproficiency"]').on('input', function() {
    //  if ($('[name="language.howlearn"]').val() !== '' && $('[name="language.overallproficiency"]').val() !== '') {
    //    $('[name="language.howlearn"]').closest('ol').siblings('.nextq').button('enable');
    //  } else {
    //    $('[name="language.howlearn"]').closest('ol').siblings('.nextq').button('disable');
    //  }
    //});
    //
    ///* Questions with checkboxes and "other" text entry */
    //$('[name="language.caregivers"], [name="language.used_growing_up"], [name="language.exposure"]').on('change', function() {
    //  if(($(this).filter(':checked').length > 0) || ($(this).siblings('[type="text"]').val() !== '')) {
    //    $(this).closest('li').children('.nextq').button('enable');
    //  } else {
    //    $(this).closest('li').children('.nextq').button('disable');
    //  }
    //});
    //$('[name="language.caregivers_other"], [name="language.used_growing_up_other"], [name="language.exposure_other"]').on('input', function() {
    //  if (($(this).val() !== '') || ($(this).siblings('[type="checkbox"]').filter(':checked') > 0)) {
    //    $(this).closest('li').children('.nextq').button('enable');
    //  } else {
    //    $(this).closest('li').children('.nextq').button('disable');
    //  }
    //});
    //
    ///* Combo of yes/no and dropdown that must be valid if "yes" */
    //$('[name="language.music_years"]').prop('disabled', 'disabled');
    //$('[name="language.music_training"]').on('change', function() {
    //  if ($(this).filter(':checked').val() === 'Yes') {
    //    $('[name="language.music_years"]').removeAttr('disabled');
    //  } else {
    //    $('[name="language.music_years"]').val('none').prop('disabled', 'disabled');
    //  }
    //});
    //// FIXME: maybe can cleanup by disabling button when "yes" is selected and renabling when dropdown changes from "none"
    //$('[name="language.music_training"]').closest('li').children('.nextq').off('click').on('click', function() {
    //  var btn = this;
    //  if($('[name="language.music_training"]:checked').val() === 'Yes') {
    //    if($('[name="language.music_years"]').val() === 'none') {
    //      alert('If you have had music training, you must specify how much.');
    //    } else {
    //      $(btn).parent().hide('slide', function() { $(this).next().show();});
    //    }
    //  } else {
    //    $(btn).parent().hide('slide', function() { $(this).next().show();});
    //  }
    //});
    //
    ///* Combo of yes/no and text entry that must be non-empty if "yes" */
    //$('[name="language.hearing_explain"]').prop('disabled', 'disabled').hide();
    //$('[name="language.hearing_problems"]').parent().off('change');
    //$('[name="language.hearing_problems"]').on('change', function() {
    //  if ($(this).filter(':checked').val() === 'Yes') {
    //    $('[name="language.hearing_explain"]').removeAttr('disabled').show();
    //    $(this).closest('li').children('.nextq').button('disable');
    //  } else {
    //    $('[name="language.hearing_explain"]').val('').prop('disabled', 'disabled').hide();
    //    $(this).closest('li').children('.nextq').button('enable');
    //  }
    //});
    //$('[name="language.hearing_explain"]').on('input', function() {
    //  if($(this).val() === '') {
    //    $(this).closest('li').children('.nextq').button('disable');
    //  } else {
    //    $(this).closest('li').children('.nextq').button('enable');
    //  }
    //});

    $('#endrsrb').on('click', function() {
        $('#rsrb').hide('slide', function() {
            $('#comment').show('slide', function() {
                $('#submit').show('slide', function () {
                    $(this).button('enable');
                });
                $('[name="comment"]').focus();
            });
        });
    });
});
