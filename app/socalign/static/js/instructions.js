$('#intro').children('.listcontent').show(500);

$('.instructionlistitem h3 a').on('click', function() {
  $(this).parent().siblings('.listcontent').toggle(500);
});

$('.instructionbutton').on('click', function(e) {
  e.stopPropagation();
  $(this).parents('.listcontent').hide(500);
  $(this).parents('.instructionlistitem').next().children('.listcontent').show(500, function() {
    var pos = $(this).offset();
    $('html,body').animate({scrollTop: pos.top}, 500);
  });
});
