/*
This just loads when in preview mode on MTurk to check browser features
*/
$(document).ready(function() {

  if (!Modernizr.audio) {
    $('#oldBrowserMessage').addClass('overlay').show();
  }
});
