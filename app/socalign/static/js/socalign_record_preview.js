/*
This just loads when in preview mode on MTurk to check browser features
*/
$(document).ready(function() {

  var hasHTML5Audio = Modernizr.audio;
  var hasgetUserMedia = Modernizr.getusermedia;
  var hasCanvas = Modernizr.canvas && Modernizr.canvastext;

  if (!(hasHTML5Audio && hasCanvas && hasgetUserMedia)) {
    $('#oldBrowserMessage').addClass('overlay').show();
  }
});
