function register(field, answer) {
    if (typeof console !== "undefined" && typeof console.log !== "undefined") {
        console.log('Registering: ' + field + ' with value: ' + answer);
    }
    $('<input>').attr({
        type: 'hidden',
        id: field,
        name: field
    }).val(answer).appendTo('form');
    //document.getElementById(field).value = answer;
    return true;
}

function onExperimentEnd() {
    $('#expt_survey').show();
    $('#expt_survey ul li:first-child').show();
}

function log(args) {
    if (typeof console !== "undefined" && typeof console.log !== "undefined") {
        console.log(args);
    }
}

var flashref = null;
function flashLoadedCB(e) {
    if (e.success) {
        flashref = e.ref;
    } else {
        $('#flashcontent').html('Failed to load flash content.');
    }
}

var flashvars = {exp: experiment.get('condition'), console: "true", path: '/stimuli/mtSPR/'};
var params = {allowScriptAccess: "always", menu: "false", bgcolor:"#ffffff", quality: "high"};

$(document).ready(function() {
    if (Modernizr.geolocation) {
        hlpgmaps.getLocation();
    } else {
        $('#instructions').hide('slide', 1, function() {
            $('#oldBrowserMessage').show();
        });

    }
    $('input[name="browserid"]').val(navigator.userAgent);
    $('input[name="flashversion"]').val(swfobject.ua.pv.join("."));
    $('button#endinstr').on('click', function(){
        $('#instructions').hide('slide', function() {
            $("#current_state").show('slide');
        });
    });
    $('[name="reported_state"]').on('change', function() {
        var loc = $(this).val();
        if (loc === "NA") {
            $('button#gogeolocate').attr('disabled', 'disabled');
        } else {
            var hit_states = experiment.get('states');
            var hit_abbrevs = experiment.get('abbrevs');
            $('button#gogeolocate').removeAttr('disabled');
            if (hit_abbrevs.indexOf(loc) == -1) {
                alert("This HIT is for " + hit_states.join(', ') + " and you say you're in " + loc + "\nAre you sure you want to proceed?");
            }
        }
    });
    $('button#gogeolocate').on('click', function() {
        $("#current_state").hide('slide', function() {
            $('#map_container').show('slide');
        });
    });
    $('button#dismiss_map').on('click', function() {
        $('#map_container').hide('slide', function() {
            swfobject.embedSWF("https://maple.lrdc.pitt.edu/stimuli/mtSPR/FlexSPR4.swf",
                "flashcontent", "950", "600", "24.0.0", null,
                flashvars, params, {}, function(e) {flashLoadedCB(e);});
        });
    });
    $('[name="badlocation"]').on('change', function() {
        if ($(this).prop('checked')) {
            $('[name="reportedlocation"]').removeAttr('disabled');
            $('[name="reportedlocation"]').focus();
        } else {
            $('[name="reportedlocation"]').val('');
            $('[name="reportedlocation"]').attr('disabled', 'disabled');
        }
    });
    $('.yesno,.optscale').buttonset();

    $('.yesno,.optscale').on('change', function() {$(this).siblings('.nextq').removeAttr('disabled');});
    $('.nextq').on('click', function() {
        if($(this).parent()[0] === $('#expt_survey li:last-child')[0]) {
            $('#expt_survey').hide('slide', function() {
                $('#lang_survey').show('slide', function() {
                    $('#lang_survey ul li:first-child').show();
                });
            });
        } else if ($(this).parent()[0] === $('#lang_survey li:last-child')[0]) {
            $('#lang_survey').hide('slide', function() {
                $('#rsrb').show();
            });
        } else {
            $(this).parent().hide('slide', function() {
                $(this).next().show('slide', function() { $(this)[0].scrollIntoView();});
            });
        }
    });
    $('input[name="postexperiment.native_language"]')
        .add('[name="postexperiment.first_language"]')
        .add('[name="postexperiment.simultaneous_language"]')
        .add('[name="postexperiment.where_learn_english"]')
        .add('[name="postexperiment.livenow"]')
        .on('keypress', function() {
            if($(this).val() !== '') {
                $(this).parent().parent().children('.nextq').removeAttr('disabled');
            } else {
                $(this).parent().parent().children('.nextq').attr('disabled', 'disabled');
            }
        });
    //$('[name="postexperiment.time_in_pitt"]')
    //    .add('[name="postexperiment.simultaneous_language"]')
    //    .parent().siblings('.nextq').removeAttr('disabled');
    $('#endrsrb').on('click', function() {
        $('#rsrb').hide('slide');
        $("#comment").show('slide', function(){$('#commentarea').focus();});
        $("#submit").show('slide', 1, function() {
            $(this).removeAttr('disabled');
        });
    });
});
