function register(field, answer) {
    if (typeof console !== "undefined" && typeof console.log !== "undefined") {
        console.log('Registering: ' + field + ' with value: ' + answer);
    }
    $('<input>').attr({
        type: 'hidden',
        id: field,
        name: field
    }).val(answer).appendTo('form');
    //document.getElementById(field).value = answer;
    return true;
}

function onExperimentEnd() {
    $('#rsrb').show();
    $('#expt_survey ul li:first-child').show();
}

function log(args) {
  if (!_.isUndefined(console) && !_.isUndefined(console.log)) {
    console.log(args);
  }
}

function flashLoadedCB(e) {
  if (!e.success) {
    var error_string = '';
    if (_.isEqual(swfobject.ua.pv, [0, 0, 0])) {
      error_string = '<p style="font-weight bold; color: red;">Flash plugin is not installed or is disabled</p>';
      document.getElementById('fixflash').style.display = 'block';
    } else if (swfobject.ua.pv[0] < 20) {
      error_string = '<p style="font-weight bold; color: red;">Flash plugin is too old: ' + swfobject.ua.pv.join('.') +
        ' is installed but 20.0.0 or newer is required. Please update your Flash player.</p>';
    } else {
      error_string = '<p style="font-weight bold; color: red;">Failed to load flash content.</p>';
    }
    document.getElementById('flashcontent').innerHTML = error_string;
  }
}

var flashvars = {exp: window.condition, console: "true", path: "/mturk/mtSPR/"};

$(document).ready(function() {
    $('input[name="browserid"]').val(navigator.userAgent);
    $('input[name="flashversion"]').val(swfobject.ua.pv.join('.'));
    var params = {allowScriptAccess: "always", menu: "false", bgcolor:"#ffffff", quality: "high"};
    $('button#endinstr').on('click', function(){
        $('#instructions').hide(function() {
          swfobject.embedSWF(swfurl,
            'flashcontent', '950', '600', '20.0.0', null,
            flashvars, params, {}, function (e) {
              flashLoadedCB(e);
            });
        });
    });

    $('#endrsrb').on('click', function() {
        $('#rsrb').hide('slide');
        $('#comment').show(function(){$('#commentarea').focus();});
        $('#submit').show(function() {
            $(this).removeAttr('disabled');
        });
    });
});
