#
# Author: Andrew Watts <awatts2@ur.rochester.edu>
#
# Copyright (c) 2014-2016, Andrew Watts and the University of Rochester BCS Department
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# * Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from __future__ import print_function
from __future__ import absolute_import

from functools import partial
import os.path
from six.moves import configparser
from six.moves.configparser import NoSectionError, NoOptionError
from flask import Blueprint, render_template, request, abort
from jinja2.exceptions import TemplateNotFound

try:
    from config import GMAPS_API_KEY
except ImportError:
    GMAPS_API_KEY = 'key_import_failed'
from app.utils import get_or_create_worker_assignment, random_list, static_list

basepath = os.path.dirname(__file__)

mod = Blueprint('sprrunner',
                __name__,
                url_prefix='/sprrunner',
                template_folder='templates',
                static_folder='static',
                static_url_path='/static/sprrunner')


@mod.route('/')
def web_spr_server():
    """
    WSGI compatible class to dispatch pages for the experiment
    """

    condition = None
    required_keys = {'assignmentId', 'hitId', 'experiment'}
    key_error_msg = 'Missing parameter: {0}. Required keys: {1}'

    # if 'debug' in request.args:
    #     app.debug = request.args['debug'] == '1'
    # forcelist = None
    # if 'list' in request.args:
    #     forcelist = request.args['list']

    missing_keys = set()
    for k in required_keys:
        if k not in request.args:
            missing_keys.add(k)
    if missing_keys:
        abort(400, {'message': key_error_msg.format(missing_keys, required_keys)})

    cfg = configparser.ConfigParser()
    config_expt = request.args['experiment']
    if 'region' in request.args:
        config_expt += '-{}'.format(request.args['region'])
    cfg_file = cfg.read(os.path.join(basepath, 'experiment_config', '{}.cfg'.format(config_expt)))
    if not cfg_file:
        abort(400, dict(message='{}.cfg does not exist'.format(config_expt)))
    try:
        listtype = cfg.get('lists', 'type')
    except (NoSectionError, NoOptionError) as e:
        abort(400, {'message': 'In {}: {}'.format(cfg_file, e.message)})

    if listtype == 'filtered':
        lsubset = cfg.get('lists', 'lists').strip().split(',')
    elif listtype == 'static':
        lname = cfg.get('lists', 'list')

    template_file = 'webspr.jinja2'
    if cfg.has_section('template'):
        template_file = cfg.get('template', 'override')
    # print(template_file)

    if 'region' in request.args:
        try:
            states = {
                'names': cfg.get('states', 'names').strip().split(','),
                'abbrevs': cfg.get('states', 'abbrevs').strip().split(',')
            }
        except (NoSectionError, NoOptionError) as e:
            abort(400, {'message': 'In {}: {}'.format(cfg_file, e.message)})
    else:
        states = {'names': [], 'abbrevs': []}

    in_preview = request.args['assignmentId'] == 'ASSIGNMENT_ID_NOT_AVAILABLE'

    formtype = None
    if 'dest' in request.args:
        if request.args['dest'] in ('mturk', 'sandbox'):
            formtype = request.args['dest']
        else:
            abort(400, {'message': "{} is not a valid form destination".format(request.args['dest'])})

    # print('Debug: {}, Forcelist: {}'.format(debug, forcelist))

    assignment = None
    if not in_preview:
        if 'workerId' not in request.args:
            missing_keys.add('workerId')
            required_keys.add('workerId')
            abort(400, {'message': key_error_msg.format(missing_keys, required_keys)})
        get_assignment = partial(get_or_create_worker_assignment,
                                 request.args['experiment'],
                                 request.args['workerId'],
                                 request.args['assignmentId'])
        if listtype == 'random':
            assignment = get_assignment('random')
        elif listtype == 'filtered':
            assignment = get_assignment('filtered', lsubset=lsubset)
        elif listtype == 'static':
            assignment = get_assignment('static', listname=lname)

    if assignment:
        condition = "{}/{}".format(request.args['experiment'], assignment.triallist.name)
    # FIXME: Whatever the hell I was doing here, figure out how to do it right with Flask
    #     if app.debug and forcelist:
    #         formtype = 'testing'  # should never need to debug on live
    #         listname = forcelist
    #         try:
    #             condref = TrialList.query.filter_by(experiment=request.args['experiment'], name=listname).one()
    #         except:
    #             abort(400, {'message': "Could not find list {} for {}".format(listname, request.args['experiment'])})
    #         condition = condref.name
    #     else:
    #         condition = worker.triallist.name

    # if (worker is not None and worker.completed):
    #     render_template('sorry.html')
    # else:
    try:
        return render_template(template_file,
                               amz=request.args,
                               experiment=request.args['experiment'],
                               condition=condition,
                               formtype=formtype,
                               # debug=1 if app.debug else 0,
                               jqueryui=1,  # FIXME: this should be set programmatically
                               gmaps_key=GMAPS_API_KEY,  # FIXME: this isn't always necessary
                               states=states,
                               # on preview, don't bother loading heavy flash assets
                               preview=in_preview)
    except TemplateNotFound as e:
        abort(400, {'message': '{}'.format(e.message)})


@mod.route('/demo/<experiment>')
@mod.route('/demo/<experiment>/<int:listid>')
@mod.route('/demo/<experiment>/<region>')
@mod.route('/demo/<experiment>/<region>/<listname>')
def spr_demo(experiment, region=None, listname=None, listid=None):
    amz = {
        'experiment': experiment,
        'assignmentId': 'NA',
        'region': region
    }

    cfg = configparser.ConfigParser()
    config_expt = experiment
    if region:
        config_expt += '-{}'.format(region)
    cfg_file = cfg.read(os.path.join(basepath, 'experiment_config', '{}.cfg'.format(config_expt)))
    if not cfg_file:
        abort(400, dict(message='{}.cfg does not exist'.format(config_expt)))

    template_file = 'webspr.jinja2'
    if cfg.has_section('template'):
        template_file = cfg.get('template', 'override')
    # print(template_file)

    if region:
        try:
            states = {
                'names': cfg.get('states', 'names').strip().split(','),
                'abbrevs': cfg.get('states', 'abbrevs').strip().split(',')
            }
        except (NoSectionError, NoOptionError) as e:
            abort(400, {'message': 'In {}: {}'.format(cfg_file, e.message)})
    else:
        states = {'names': [], 'abbrevs': []}

    if listname:
        triallist = static_list(experiment, listname)
    elif listid:
        triallist = static_list(experiment, listid)
    else:
        triallist = random_list(experiment)
    condition = "{}/{}".format(experiment, triallist.name)

    try:
        return render_template(template_file,
                               amz=amz,
                               experiment=experiment,
                               condition=condition,
                               formtype='local',
                               # debug=1 if app.debug else 0,
                               jqueryui=1,  # FIXME: this should be set programmatically
                               gmaps_key=GMAPS_API_KEY,  # FIXME: this isn't always necessary
                               states=states,
                               # on preview, don't bother loading heavy flash assets
                               preview=False)
    except TemplateNotFound as e:
        abort(400, {'message': '{}'.format(e.message)})
