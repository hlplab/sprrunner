#!/usr/bin/env python

from sqlalchemy.exc import IntegrityError
from app import app, db, user_datastore
from app.models import User

app.config['QUERYINSPECT_ENABLED'] = False

role_name = input("Role name: ")
role_desc = input("Role description: ")

with app.app_context():
    role = user_datastore.create_role(name=role_name, description=role_desc)
    try:
        db.session.commit()
    except IntegrityError:
        print('Role {} already exists!'.format(role.name))
    else:
        print('Created role {}'.format(role.name))

    users = {u.id: u.email for u in User.query.all()}
    if users:
        print(users)
        add_users = input("Enter comma separated ids of users to add to role: ")
        for uid in [x for x in add_users.split(',')]:
            try:  # in case it's a quoted int, try to convert it
                uid = int(uid)
            except ValueError:  # it's actually an email address
                pass
            user = user_datastore.get_user(uid)
            user_datastore.add_role_to_user(user, role)
            try:
                db.session.commit()
            except IntegrityError:
                print("Failed to add {} to {}".format(user, role))
            else:
                print("Added {} to {}".format(user, role))
