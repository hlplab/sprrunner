import os
basedir = os.path.abspath(os.path.dirname(__file__))

SECRET_KEY = b''

DEBUG = True
PROPAGATE_EXCEPTIONS = True

SERVERNAME = ''
APPLICATION_ROOT = ''

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'sprrunner.sqlite')
SQLALCHEMY_TRACK_MODIFICATIONS = False

SECURITY_PASSWORD_SALT = b''  # if you use a real hash, you need to specify a salt.
SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'  # default is plain text!

AWS_ACCESS_KEY = ''
AWS_SECRET_KEY = ''
SQS_REGION = 'us-east-1'  # leaving this in the example, b/c it's the only region MTurk is in so it's simpler to use it
SQS_QUEUE = ''

GMAPS_API_KEY = ''
