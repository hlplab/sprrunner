import os, sys
#from werkzeug.debug import DebuggedApplication
PROJECT_DIR='/usr/local/mturk-apps/sprrunner_env/'
activate_this = os.path.join(PROJECT_DIR, 'bin', 'activate_this.py')
execfile(activate_this, dict(__file__=activate_this))
sys.path.insert(0, os.path.join(PROJECT_DIR, 'sprrunner'))
from app import app as application

if __name__ == '__main__':
    #application = DebuggedApplication(application, evalex=True)
    application.run()
